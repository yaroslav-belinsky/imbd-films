module.exports = function(grunt) {
    grunt.initConfig({
        uglify: {
            my_target: {
                files: {
                    'www/lib/libs.js': [
                        'www/lib/ionic/js/ionic.bundle.js',
                        'www/lib/angular-local-storage/dist/angular-local-storage.min.js',
                        'www/lib/Chart.js/Chart.js',
                        'www/lib/angular-chart.js/dist/angular-chart.js',
                        'www/lib/ionic-image-lazy-load/ionic-image-lazy-load.js'
                    ],
                    'www/js/app.min.js': [
                        'www/js/app.js',
                        'www/js/controllers.js',
                        'www/js/services.js'
                    ]
                }
            }
        }

    });

    grunt.registerTask('default', ['concat', 'uglify']);

    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-uglify');
};