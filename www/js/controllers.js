angular.module('starter.controllers', [])

.controller('TopCtrl', ['$scope', 'TopService', '$ionicLoading', '$ionicModal', '$ionicHistory', '$sce', function($scope, TopService, $ionicLoading, $ionicModal, $ionicHistory, $sce) {
        $ionicLoading.show({
            template: 'Loading...'
        });
        $scope.viewName = "Top 20 movies";
        TopService.all().then(function(data) {
            $scope.movies = TopService.getDataLocal(data.movies);
            $ionicLoading.hide();
        }, function(err) {
            $scope.movies = TopService.getDataLocal();
            console.log(err);
            $scope.err = err;
            $ionicLoading.hide();
        });
        
        
        $scope.favoriteChange = function(movie) {
            console.log(movie);
            TopService.favoriteChange(movie, $scope.movies);
        }
        $scope.trustSrc = function(src) {
            return $sce.trustAsResourceUrl(src);
          }
        $ionicModal.fromTemplateUrl('templates/trailer-modal.html', {
            scope: $scope,
            animation: 'slide-in-up'
        }).then(function(modal) {
            $scope.modal = modal;
        });
        $scope.openModal = function(currentVideo) {
            var trailer = TopService.getTrailer(currentVideo);
            currentVideo.trailer = trailer[0];
            $scope.currentVideo = currentVideo;
            $scope.modal.show();
        };
        $scope.closeModal = function() {
            $scope.modal.hide();
        };

    }])
    .controller('FavoritesCtrl', ['$scope', 'TopService', '$ionicLoading', '$timeout', '$ionicModal', '$sce', function($scope, TopService, $ionicLoading, $timeout, $ionicModal, $sce) {
        console.log('FavoritesCtrl');
        $ionicLoading.show({
            template: 'Loading...'
        });
        $scope.viewName = "Favorites";
        $scope.movies = TopService.getFavorites();
        $timeout(function() {
            $ionicLoading.hide();
        },500);
        $scope.favoriteChange = function(movie) {
            console.log(movie);
            TopService.favoriteChange(movie, $scope.movies);
            $scope.movies = TopService.getFavorites();
        }
        $scope.trustSrc = function(src) {
            return $sce.trustAsResourceUrl(src);
          }
        $ionicModal.fromTemplateUrl('templates/trailer-modal.html', {
            scope: $scope,
            animation: 'slide-in-up'
        }).then(function(modal) {
            $scope.modal = modal;
        });
        $scope.openModal = function(currentVideo) {
            var trailer = TopService.getTrailer(currentVideo);
            currentVideo.trailer = trailer[0];
            $scope.currentVideo = currentVideo;
            $scope.modal.show();
        };
        $scope.closeModal = function() {
            $scope.modal.hide();
        };
    }])

.controller('ChartsCtrl', function($scope, TopService, $ionicLoading) {
    $ionicLoading.show({
            template: 'Loading...'
        });
    TopService.getChartsData().then(function(moviesDecades) {
        $scope.moviesDecades = moviesDecades;
        $scope.moviesDecades.options = { segmentShowStroke : true};
        $scope.moviesDecades.legend = true;
        $scope.series = ['Series A'];
        $scope.data = [moviesDecades.counts];
        console.log(moviesDecades);
        $ionicLoading.hide();
    }, function(err) {
        console.log(err);
        $ionicLoading.hide();
    });
});