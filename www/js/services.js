angular.module('starter.services', [])
    .factory('TopService', ['$http', '$q', 'localStorageService', '$filter', '$ionicHistory', '$ionicPopup', function($http, $q, localStorageService, $filter, $ionicHistory, $ionicPopup) {
        var TopServiceFactory = {};
        var _trailersBase = [{
            "idIMDB": "tt0111161",
            "trailerUrl": "https://www.youtube.com/embed/6hB3S9bIaco",
        }, {
            "idIMDB": "tt0068646",
            "trailerUrl": "https://www.youtube.com/embed/sY1S34973zA"
        }, {
            "idIMDB": "tt0071562",
            "trailerUrl": "https://www.youtube.com/embed/8PyZCU2vpi8"
        }, {
            "idIMDB": "tt0468569",
            "trailerUrl": "https://www.youtube.com/embed/EXeTwQWrcwY"
        }, {
            "idIMDB": "tt0468569",
            "trailerUrl": "https://www.youtube.com/embed/EXeTwQWrcwY"
        }, {
            "idIMDB": "tt0050083",
            "trailerUrl": "https://www.youtube.com/embed/fSG38tk6TpI"
        }, {
            "idIMDB": "tt0108052",
            "trailerUrl": "https://www.youtube.com/embed/JdRGC-w9syA"
        }, {
            "idIMDB": "tt0110912",
            "trailerUrl": "https://www.youtube.com/embed/s7EdQ4FqbhY"
        }, {
            "idIMDB": "tt0060196",
            "trailerUrl": "https://www.youtube.com/embed/WCN5JJY_wiA"
        }, {
            "idIMDB": "tt0167260",
            "trailerUrl": "https://www.youtube.com/embed/r5X-hFf6Bwo"
        }, {
            "idIMDB": "tt0137523",
            "trailerUrl": "https://www.youtube.com/embed/SUXWAEX2jlg"
        }];
        var _showAlert = function() {
            var alertPopup = $ionicPopup.alert({
                title: 'Attention!',
                template: 'Connection failed try again later!'
            });
            alertPopup.then(function(res) {
                console.log('Thank you for not eating my delicious ice cream cone');
            });
        };
        var _all = function() {
            var deferred = $q.defer();
            $http.jsonp(serviceBase + '/top?end=20&token=' + Token + '&format=json&data=1&callback=JSON_CALLBACK').then(function(results) {
                deferred.resolve(results.data.data);
            }, function(error) {
                _showAlert();
                deferred.reject(error);
            });
            return deferred.promise;
        };
        var _getTrailer = function(movie) {

            return $filter('filter')(_trailersBase, {
                idIMDB: movie.idIMDB
            }, true);

        }
        var _getDataLocal = function(movies) {
            if (localStorageService.isSupported && localStorageService.get('moviesLocal') !== undefined && localStorageService.get('moviesLocal') !== null) {
                var moviesLocal = localStorageService.get('moviesLocal');
                if (movies !== undefined) {
                    var moviesDifferance = false;
                    for (var i = 0; i < movies.length; i++) {
                        var exist = $filter('filter')(moviesLocal, {
                            idIMDB: movies[i].idIMDB
                        }, true);

                        if (exist.length == 0) {
                            moviesDifferance = true;
                            movies[i].favorite = false;
                        } else {
                            movies[i].favorite = exist[0].favorite;
                        }

                        if (moviesDifferance) {
                            localStorageService.set('moviesLocal', movies);
                        }


                    }
                    return movies;
                } else {
                    return moviesLocal;
                }
            } else {
                if (movies !== undefined && localStorageService.isSupported) {
                    for (var i = 0; i < movies.length; i++) {
                        movies[i].favorite = false;
                    }
                    localStorageService.set('moviesLocal', movies);
                    return movies;
                } else {
                    return false;
                }
            }
        };
        var _saveDataToLocal = function(movies) {
            if (movies !== undefined && localStorageService.isSupported) {
                localStorageService.set('moviesLocal', movies);
            }
        };
        var _getFavorites = function() {
            if (localStorageService.isSupported && localStorageService.get('moviesLocal') !== undefined && localStorageService.get('moviesLocal') !== null) {
                var moviesLocal = $filter('filter')(localStorageService.get('moviesLocal'), {
                    favorite: true
                }, true);
                return moviesLocal;
            } else {
                return false;
            }
        };
        var _favoriteChange = function(movie, movies) {
            movie.favorite = !movie.favorite;
            _saveDataToLocal(movies);
            $ionicHistory.clearHistory();
            $ionicHistory.clearCache();
        }
        var _getChartsData = function() {
            var deferred = $q.defer();
            _all().then(function(results) {
                deferred.resolve(_sortMovies(results.movies));
            }, function(error) {
                var movies = _getDataLocal();
                if (movies !== undefined) {
                    deferred.resolve(_sortMovies(movies));
                } else {
                    deferred.reject(error);
                }
            });
            return deferred.promise;
        }
        var _sortMovies = function(movies) {
            var minYear = 3000;
            var minDecade = 0;
            var maxDecade = 0;
            var maxYear = 0;
            for (var i = 0; i < movies.length; i++) {
                if (movies[i].year < minYear) {
                    minYear = movies[i].year;
                }
                if (movies[i].year > maxYear) {
                    maxYear = movies[i].year;
                }
            };
            minDecade = parseInt(minYear.toString().slice(0, 3) + 0);
            maxDecade = parseInt(maxYear.toString().slice(0, 3) + 0) + 10;
            var newMoviesDecade = new Array();
            var newMoviesCount = new Array();
            for (var j = minDecade; j < maxDecade; j = j + 10) {
                newMoviesDecade.push(j);
                newMoviesCount.push($filter('filter')(movies, {
                    year: j.toString().slice(0, 3)
                }).length);
            };
            newMovies = {
                'decades': newMoviesDecade,
                'counts': newMoviesCount
            };
            return newMovies;
        }

        TopServiceFactory.all = _all;
        TopServiceFactory.getTrailer = _getTrailer;
        TopServiceFactory.getDataLocal = _getDataLocal;
        TopServiceFactory.saveDataToLocal = _saveDataToLocal;
        TopServiceFactory.getFavorites = _getFavorites;
        TopServiceFactory.favoriteChange = _favoriteChange;
        TopServiceFactory.getChartsData = _getChartsData;
        return TopServiceFactory;

    }]);